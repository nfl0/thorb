.PHONY: build lint format test test-watch shell

APP_NAME=thorb
IMAGE_NAME=registry.gitlab.com/nfl0/${APP_NAME}
LOGLEVEL?=INFO
RUNE?=BNB.RUNE-B1A
SIMULATION?=true
THORCHAIN?=http://67.205.166.241:1317
MAX_PROFITS?=true
MNEMONIC?=cross stay slide level crush civil link require degree detail young bubble
DOCKER_OPTS = -p 8001:8001 -e THORCHAIN=${THORCHAIN} -e SIMULATION=${SIMULATION} -e MAX_PROFIT=${MAX_PROFITS} -e MNEMONIC="${MNEMONIC}" -e RUNE=${RUNE} -e LOGLEVEL=${LOGLEVEL} -e PYTHONPATH=/app -v ${PWD}:/app -w /app

clean:
	rm *.pyc

build:
	@docker build .

lint:
	@docker run --rm -v ${PWD}:/app pipelinecomponents/flake8:latest flake8

format:
	@docker run --rm -v ${PWD}:/app cytopia/black /app

test:
	@docker run ${DOCKER_OPTS} ${IMAGE_NAME} python -m unittest tests/test_*

test-coverage:
	@docker run ${DOCKER_OPTS} ${IMAGE_NAME} coverage run -m unittest tests/test_*

test-coverage-report:
	@docker run ${DOCKER_OPTS} ${IMAGE_NAME} coverage report -m

test-watch:
	@PYTHONPATH=${PWD} ptw .

pnl:
	@docker run ${DOCKER_OPTS} --rm --name ${APP_NAME} ${IMAGE_NAME} python src/thorb.py pnl

run:
	@docker run ${DOCKER_OPTS} --rm --name ${APP_NAME} ${IMAGE_NAME} python src/thorb.py

run-daemon:
	@docker run ${DOCKER_OPTS} -d --name ${APP_NAME} --restart unless-stopped ${IMAGE_NAME} python src/thorb.py

restart-daemon:
	@docker restart ${APP_NAME}

logs:
	@docker logs -f --tail 100 ${APP_NAME}

shell:
	@docker run ${DOCKER_OPTS} -it ${IMAGE_NAME} sh


# ------------------------------- GitLab ------------------------------- #

docker-gitlab-login:
	docker login -u ${CI_REGISTRY_USER} -p ${CI_REGISTRY_PASSWORD} ${CI_REGISTRY}

docker-gitlab-push:
	docker push ${IMAGE_NAME}:latest

docker-gitlab-build: build
# ------------------------------------------------------------------ #
