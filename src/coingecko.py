import logging
from datetime import datetime

from common import HttpClient


class Coingecko(HttpClient):
    """
    A local simple implementation of coingecko
    """

    def __init__(self, base_url):
        self.base_url = base_url
        self.ids = {
            "BNB.RUNE-B1A": "thorchain",
            "BNB.AVA-645": "concierge-io",
            # "BNB.BCH-1FD": "bitcoin-cash",
            "BNB.BNB": "binancecoin",
            # "BNB.BOLT-4C6": "bolt",
            "BNB.BTCB-1DE": "bitcoin-bep2",
            # "BNB.BULL-BE4": None,
            "BNB.BUSD-BD1": "binance-usd",
            # "BNB.BZNT-464": "bezant",
            # "BNB.CAN-677": "canyacoin",
            # "BNB.DOS-120": "dos-network",
            # "BNB.EOSBULL-F0D": "3x-long-eos-token",
            "BNB.ETH-1C9": "ethereum",
            # "BNB.ETHBULL-D33": "3x-long-ethereum-token",
            # "BNB.FRM-DE7": "ferrum-network",
            # "BNB.FTM-A64": "fantom",
            # "BNB.MITX-CAA": "morpheus-labs",
            # "BNB.SHR-DB6": "sharetoken",
            # "BNB.SWINGBY-888": "swingby",
            # "BNB.TWT-8C2": "trust-wallet-token",
        }

    def get_coin_id(self, asset):
        if asset in self.ids:
            return self.ids[asset]
        return None

    def get_coins(self):
        return self.fetch("/coins/list")

    def get_price(self, asset, ts=None):
        coin_id = self.get_coin_id(asset)
        if coin_id is None:
            return 0

        resp = self.fetch("/simple/price", {"ids": coin_id, "vs_currencies": "usd"})
        if coin_id not in resp:
            return 0

        price = resp[coin_id]["usd"]
        return price
