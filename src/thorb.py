#!/usr/bin/env python

import logging
import os
import sys
import time

from common import Asset, Coin, OrderBook
from config import Configuration
from thorchain import Thorchain, Pool
from binance import MockBinance, Binance
from binance_dex import BinanceDex
from coingecko import Coingecko

# Init logging
logging.basicConfig(
    format="%(asctime)s | %(levelname).4s | %(message)s",
    level=os.environ.get("LOGLEVEL", "INFO"),
)

RUNE=Asset("BNB.RUNE-B1A")
INCOME_VALUE=4

class Samaritan:
    def __init__(self, config):
        self.cfg = config

        self.thorchain_client = Thorchain(self.cfg.thorchain)
        self.thorchain_address = self.thorchain_client.asgard_address()
        self.gecko = Coingecko("https://api.coingecko.com/api/v3")
        self.binance_dex = BinanceDex()
        self.binance = Binance()

    def eval_trade(self, out_hash):
        out_tx = self.binance.get_transaction(out_hash)
        in_hash = out_tx['memo'].split(":")[-1]
        in_tx = self.binance.get_transaction(in_hash)
        in_coin = in_tx['inputs'][0]['coins'][0]
        out_coin = out_tx['outputs'][0]['coins'][0]
        in_price = self.gecko.get_price(f"BNB.{in_coin['denom']}")
        out_price = self.gecko.get_price(f"BNB.{out_coin['denom']}")
        pnl = in_price * (int(in_coin['amount']) / 1e8) - out_price * (int(out_coin['amount']) / 1e8)
        logging.info(f"Trade PNL: ${round(pnl, 2)}")

    def pnl(self, printer=True):
        balances = self.binance.balance()

        funds = {
            "BNB.BNB": -500.1 * 1e8,
            "BNB.RUNE-B1A": -196000 * 1e8,
            "BNB.BTCB-1DE": -1.5 * 1e8,
            "BNB.BUSD-BD1": -15000 * 1e8,
            "BNB.AVA-645": -94033944844,
            "BNB.ETH-1C9": -30.04387498 * 1e8
        }
        
        # minus funding transactions
        for b in balances:
            if b.asset not in funds:
                funds[b.asset] = 0
            funds[b.asset] += b.amount

        if printer:
            logging.info(">>>> PnL Stats:")
        profit = 0
        for key, value in funds.items():
            price = self.gecko.get_price(key)
            profit += (value / 1e8) * price
            if printer:
                logging.info(f"\t{key}:\t{round(value / 1e8, 2)}")

        if printer:
            logging.info(f">>>> Total: ${'{0:,.2f}'.format(profit)}")
        return profit, balances


    def run(self):
        self.pnl()
        total_profit = 0
        swap_count = 0
        last_txs = {}
        profit_cache = {}
        while True:
            logging.info("=======================================")
            if self.thorchain_client.halted():
                logging.info(">>>> Trading is halted...")
                continue
            pools = self.thorchain_client.list_pools()
            if len(pools) == 0:
                logging.info(">>>> No pools in thorchain....")
            new_address = self.thorchain_client.asgard_address()
            if new_address != self.thorchain_address:
                self.thorchain_address = new_address
                logging.info(f"New Asgard Address: {self.thorchain_address}")

            self.binance_dex.update_markets()  # update markets routinely
            rune_price = self.gecko.get_price("BNB.RUNE-B1A")
            if rune_price == 0:
                continue
            min_profit = rune_price * INCOME_VALUE

            # get our balances
            pnl, balances = self.pnl(False)
            rune_balance = 0
            # remove 1 BNB for gas purposes
            for i, b in enumerate(balances):
                if b.asset == "BNB.BNB":
                    balances[i].amount = max(balances[i].amount - (0.5 * 1e8), 0)
                if b.asset == "BNB.RUNE-B1A":
                    rune_balance = b.amount

            logging.info(f"STATS: Profit: ${'{0:,.2f}'.format(total_profit)}, Swaps {swap_count:,}")
            for pool in pools:
                if pool.status.lower() != "enabled":
                    continue

                # if we already have a tx for this pool waiting to be processed, skip this pool
                if pool.asset in last_txs and not self.cfg.simulation:
                    if last_txs[pool.asset] is not None:
                        try:
                            tx = self.thorchain_client.get_transaction(last_txs[pool.asset])
                        except Exception:
                            continue
                        logging.info(f"Pool {pool.asset} is waiting for tx {last_txs[pool.asset]}")
                        if 'status' not in tx:
                            continue
                        if tx['status'] == "refund":
                            logging.info(f"Refunded Tx: {last_txs[pool.asset]}")
                        if tx['status'] == "refund" or tx['status'] == 'done':
                            last_txs[pool.asset] = None
                            total_profit += profit_cache[pool.asset]
                            swap_count += 1
                            profit_cache[pool.asset] = 0
                            self.pnl()
                        else:
                            continue

                # find balance for this asset
                balance = 0
                for b in balances:
                    if b.asset == pool.asset:
                        balance = b.amount
                        break

                asset_price = self.gecko.get_price(pool.asset)
                if asset_price == 0:
                    continue
                ratio = rune_price / asset_price
                dex_price = rune_price * pool.balance_rune / pool.balance_asset
                logging.info(f"Pricing {pool.asset}: Real ${asset_price} vs DEX ${'{0:,.3f}'.format(dex_price)}: Diff {round(abs(asset_price - dex_price), 4)}")

                expected_rune = (pool.balance_asset / ratio) - pool.balance_rune
                expected_asset = (pool.balance_rune * ratio) - pool.balance_asset

                # set directional variables
                if expected_rune > 0:
                    from_asset = RUNE
                    to_asset = pool.asset
                    balance = rune_balance
                    from_price = rune_price
                    to_price = asset_price
                    expected = expected_rune
                else:
                    from_asset = pool.asset
                    to_asset = RUNE
                    from_price = asset_price
                    to_price = rune_price
                    expected = expected_asset

                if balance == 0:
                    logging.info(f"No Balance: {balance / 1e8} {from_asset}")
                    continue

                # find a swap with a min profit with the largest trade possible
                todo = {
                    "profit": None,
                    "to_sell": None,
                    "emissions": None,
                }
                for i in range(0, 10000):
                    to_sell = Coin(from_asset, (abs(expected) / 2) * (1 - (i * 0.0001)))
                    if not self.cfg.simulation and to_sell.amount > balance:
                        to_sell.amount = balance
                    emission = pool._calc_asset_emission(to_sell.amount, to_sell.asset)
                    profit = (emission / 1e8 * to_price) - (to_sell.amount / 1e8 * from_price)
                    # give ourselves a little wiggle room, take half the income in worst case scenario
                    if to_asset == RUNE:
                        emission -= INCOME_VALUE / 2 * 1e8
                    else:
                        emission -= ((INCOME_VALUE / 2) * (rune_price / asset_price)) * 1e8
                    if todo['profit'] is None or profit > todo['profit']:
                        todo['profit'] = profit
                        todo['to_sell'] = to_sell
                        todo['emissions'] = emission
                    if profit >= min_profit and pnl >= 0:
                        break
                if todo['profit'] < min_profit:
                    logging.info(f"no arbitrage opportunity (max potential profit ${round(todo['profit'], 2)})")
                else:
                    hash_id = self.swap(pool.asset, todo['to_sell'], todo['emissions'])
                    emission = Coin(to_asset, todo['emissions'])
                    logging.info(f">>> Swapping {todo['to_sell']} --> {todo['emissions']}, Profit ${round(todo['profit'], 2)}")
                    profit_cache[pool.asset] = todo['profit']
                    if hash_id is not None :
                        last_txs[pool.asset] = hash_id
                        if from_asset == RUNE:
                            rune_balance -= todo['to_sell'].amount

            # break
            time.sleep(6)


    def swap(self, pool_asset, coin, trade_target):
        try:
            if self.cfg.simulation:
                return None
            target = pool_asset if coin.asset != pool_asset else Asset("BNB.RUNE-B1A")
            self.pnl()
            resp = self.binance.transfer(
                self.binance.get_address(),
                self.thorchain_address,
                coin.asset.split(".")[1],
                int(coin.amount),
                f"SWAP:{target}::{int(trade_target)}",
            )
            if len(resp) > 0:
                if not resp[0]['ok']:
                    logging.error(resp)
                return resp[0]['hash']
        except:
            logging.exception("failed to swap")
            return None
        return None


def main():
    logging.info("Starting good thorb....")
    config = Configuration()
    arb = Samaritan(config)
    if len(sys.argv) > 1:
        if sys.argv[1] == "pnl":
            arb.pnl()
        else:
            arb.eval_trade(sys.argv[1])
        return
    arb.run()


if __name__ == "__main__":
    main()
